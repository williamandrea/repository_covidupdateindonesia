const url = 'https://indonesia-covid-19.mathdro.id/api'

const getGlobal = async () => {
  try{
    const res = await axios.get(`${url}`)
    return res.data
  }catch(err){
    return err
  }
}

export default getGlobal

