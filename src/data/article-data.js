const articles = []

const url = 'https://indonesia-covid-19.mathdro.id/api'

const getProvinsi = async () => {
  try{
    const res = await axios.get(`${url}/provinsi`)
    res.data.data.forEach(value => {
      articles.push(value)
    })
  }catch(err){
    console.log(err)
  }
}

getProvinsi()

export default articles

