const url = 'https://indonesia-covid-19.mathdro.id/api'

const cards = [
  {
    title: 'Terkonfirmasi',
    type: 'confirmed-value',
    color: 'yellow',
    value: null,
  },
  {
    title: 'Meninggal',
    type: 'deaths-value',
    color: 'red',
    value: null,
  },
  {
    title: 'Sembuh',
    type: 'recovered-value',
    color: 'turquoise',
    value: null,
  }
]

// getCards adalah function yang menghasilkan promise bila kita jalankan
// well, async function pada dasarnya kalau dijalankan ya menghasilkan promise :)
let getCards = async () => {
  try{
    const res = await axios.get(`${url}`)
    cards[0].value = res.data.jumlahKasus
    cards[1].value = res.data.meninggal
    cards[2].value = res.data.sembuh
    return cards
  }catch(err){
    return err
  }
}

// yang kita export disini, adalah hasil dari menjalankan getCards(), bukan function getCards nya
// so yang kita export adalah promise, karena getCards() akan menghasilkan promise
export default getCards();