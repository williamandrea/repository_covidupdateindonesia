import './component/jumbotron-list.js'
import './component/search-bar.js'
import './component/article-container.js'
import getJumbotronData from './data/jumbotron-data.js'
import articleData from './data/article-data.js'

const main = () => {
  const jumbotronList = document.querySelector('jumbotron-list')
  const searchBar = document.querySelector('search-bar')
  const articleContainer = document.querySelector('article-container')
  
  // getJumbotronData adalah sebuah objek promise
  // object promise bisa kita pasang function yang akan dia jalankan bila sudah selesai
  getJumbotronData.then(data => {
    jumbotronList.cards = data
  })

  const onClickButtonSearch = () => {
    // Do nothing
  }

  const scrollTop = () => {
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
  }

  const renderArticleContainer = (articleData) => {
    articleContainer.articles = articleData
  }

  renderArticleContainer(articleData)

  document.querySelector('#logo').addEventListener('click', scrollTop)
}

export default main