import './article-item.js'

class ArticleContainer extends HTMLElement{
  constructor(){
    super()
    this.shadowDOM = this.attachShadow({mode: 'open'})
  }
  
  set articles(articles){
    this._articles = articles
    this.render()
  }

  render(){
    this.shadowDOM.innerHTML = ''
    this._articles.forEach(darta=> {
      const articleItem = document.createElement('article-item')
      articleItem.article = article
      this.shadowDOM.appendChild(articleItem)
    })
  }

  renderError(message){
    this.shadowDOM.innerHTML = ''
    this.shadowDOM.innerHTML = '<h2>null article-container</h2>'
  }
}

customElements.define('article-container', ArticleContainer)