class ArticleItem extends HTMLElement{
  constructor(){
    super()
    this.shadowDOM = this.attachShadow({mode: 'open'})
  }

  set article(article){
    this._article = article
    this.render()
  }

  render(){
    
    console.log(this._article)
    this.shadowDOM.innerHTML = `
      <style>
        *{
          margin: 0;
          padding: 0;
        }
        article{
          display: flex;
          align-items: center;
          height: 150px;
          background: #1B232F;
          padding-left: 30px;
          border-radius: 10px;
          margin-bottom: 20px;
        }
        
        .id-container{
          display: flex;
          align-items: center;
          justify-content: center;
          width: 50px;
          height: 50px;
          background: white;
          border-radius: 100px;
          margin-right: 40px;
        }

        .province-id{
          color: #171B1E;;
        }
        
        .province-name{
          font-size: 40px;
          margin: 0;
        }
        
        .article-row{
          display: flex;
        }
        
        .article-info-item{
          display: flex;
          color: 'red';
          margin-right: 40px;
        } 
        
        .article-label{
          margin-right: 10px;
        }
              
        .yellow{
          color: yellow;
        }
        
        .red{
          color: red;
        }
        
        .turquoise{
          color: turquoise;
        }

        @media screen and (max-width: 600px){
          article{
            display: flex;
            align-items: center;
            height: 150px;
            background: #1B232F;
            padding-left: 30px;
            border-radius: 10px;
            margin-bottom: 20px;
          }
          
          .id-container{
            display: flex;
            align-items: center;
            justify-content: center;
            width: 50px;
            height: 50px;
            background: white;
            border-radius: 100px;
            margin-right: 40px;
          }
  
          .province-id{
            color: #171B1E;;
          }
          
          .province-name{
            font-size: 30px;
            margin: 0;
          }
          
          .article-row{
            display: flex;
            flex-direction: column;
          }
          
          .article-info-item{
            display: flex;
            color: 'red';
            margin-right: 40px;
            // background: pink;
          } 
          
          .article-label{
            margin-right: 10px;
          }
        }
      </style>
      <article>
        <div class = 'id-container'>
         <h2 class = 'province-id'>${this._article.kodeProvi}</h2>
        </div>
        <div class = article-info>
          <h2 class = 'province-name'>${this._article.provinsi}</h2>
          <div class = 'article-row'>
            <div class = 'article-info-item'>
              <h3 class = 'article-label yellow'>Terkonfirmasi: </h3>
              <h3 class = 'yellow'>${this._article.kasusPosi}</h3>
            </div>
            <div class = 'article-info-item'>
              <h3 class = 'article-label red'>Meninggal: </h3>
              <h3 class = 'red'>${this._article.kasusMeni}</h3>
            </div>
            <div class = 'article-info-item'>
              <h3 class = 'article-label turquoise'>Sembuh: </h3>
              <h3 class = 'turquoise'>${this._article.kasusSemb}</h3>
            </div>
          </div>
        </div>
      </article>
    `
  }
}

customElements.define('article-item', ArticleItem)