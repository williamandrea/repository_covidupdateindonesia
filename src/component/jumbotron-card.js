class JumbotronCard extends HTMLElement{
  constructor(){
    super()
    this.shadowDOM = this.attachShadow({mode: 'open'})
  }

  set card(card){
    this._card = card
    this.render()
  }

  render(){
    // console.log(this._card)
    this.shadowDOM.innerHTML = `
      <style>
        *{
          margin: 0;
          padding: 0;
        }
        .jumbotron-card{
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          width: 280px;
          height: 250px;
          border-radius: 10px;
          background: #1B232F;
        }

        .jumbotron-title{
          font-size: 25px;
        }

        .yellow{
          color: yellow;
        }
        
        .red{
          color: red;
        }
        
        .turquoise{
          color: turquoise;
        }

        #confirmed-value{
          margin: 0;
          font-size: 50px;
          color: yellow;
        }

        #deaths-value{
          margin: 0;
          font-size: 50px;
          color: red;
        }
        
        #recovered-value{
          margin: 0;
          font-size: 50px;
          color: turquoise;
        }
      </style>
      <div class = 'jumbotron-card'>
        <h2 class = 'jumbotron-title ${this._card.color}'>${this._card.title}</h2>
        <h2 id = '${this._card.type}'>${this._card.value}</h2>
      </div>
    `
  }
}

customElements.define('jumbotron-card', JumbotronCard)