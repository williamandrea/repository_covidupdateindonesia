class SearchBar extends HTMLElement{
  connectedCallback(){
    this.render()
  }

  set clickEvent(event){
    this._clickEvent = event
    this.render()
  }

  get value(){
    return this.querySelector('#input-search').value
  }

  render(){
    this.innerHTML = `
      <style>
        #input-search{
          font-size: 25px;
          width: 100%;
          padding-left: 10px;
          margin-right: 10px;
          border-radius: 5px;
          color: #171B1E;
          border: 0;
        }

        #button-search{
          width: 100px;
          font-size: 25px;
          /* color: #171B1E; */
          border-radius: 5px;
          color: white;
          background: #007BFF;
          background: #28A745;
          border: 0;
        }

        #button-search:hover{
          cursor: pointer;
        }

        #button-search:focus{
          outline: 0;
        }

        @media screen and (max-width: 600px){
          #input-search{
            font-size: 25px;
            width: 100%;
            padding-left: 10px;
            margin-right: 10px;
            border-radius: 5px;
            color: #171B1E;
            border: 0;
          }
    
          #button-search{
            width: 100px;
            font-size: 25px;
            border-radius: 5px;
            color: white;
            background: #007BFF;
            background: #28A745;
            border: 0;
          }
    
          #button-search:hover{
            cursor: pointer;
          }
    
          #button-search:focus{
            outline: 0;
          }
        }
      </style>
      <input id = 'input-search' type = "text" placeholder = "Cari provinsi...">
      <button id = 'button-search' >search</button>
    `

    this.querySelector('#button-search').addEventListener('click', this._clickEvent)
  }
}

customElements.define('search-bar', SearchBar)