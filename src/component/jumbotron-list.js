import './jumbotron-card.js'

class JumbotronList extends HTMLElement{
  constructor(){
    super()
    this.shadowDOM = this.attachShadow({mode: 'open'})
  }
  
  set cards(cards){
    this._cards = cards
    this.render()
  }
  
  render(){
    // console.log(this._cards)
    this.shadowDOM.innerHTML = ''
    this._cards.forEach(card => {
      const jumbotronCard = document.createElement('jumbotron-card')
      jumbotronCard.card = card
      this.shadowDOM.appendChild(jumbotronCard)
    })
  }

  renderError(message){
    this.shadowDOM.innerHTML = ''
    this.shadowDOM.innerHTML = '<h2>null jumbotron-list</h2>'
  }
}

customElements.define('jumbotron-list', JumbotronList)