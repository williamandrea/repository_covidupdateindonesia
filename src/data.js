const url = 'https://indonesia-covid-19.mathdro.id/api'

const getGlobal = async () => {
  try{
    const res = await axios.get(`${url}`)
    const confirmed = res.data.jumlahKasus
    const deaths = res.data.meninggal
    const recovered = res.data.sembuh

    document.querySelector('#confirmed-value').innerText = confirmed
    document.querySelector('#deaths-value').innerText = recovered
    document.querySelector('#recovered-value').innerText = deaths
  }catch(err){
    console.log(err)
  }
}

const getProvinsi = async () => {
  try{
    const res = await axios.get(`${url}/provinsi`)
    const data = res.data.data
    // console.log(data.data)
    // for (const u in data.data){
    //   console.log(data.data[u].kasusMeni)
    // }
    data.forEach((value) => {
      console.log(value)
    })
    // const confirmed = res.data.jumlahKasus
    // const deaths = res.data.data.meninggal
    // const recovered = res.data.data.sembuh

    // console.log(confirmed, deaths, recovered)

    // document.querySelector('#confirmed').innerText = confirmed
    // document.querySelector('#deaths').innerText = recovered
    // document.querySelector('#recovered').innerText = deaths
  }catch(err){
    console.log(err)
  }

}

getGlobal()
getProvinsi()